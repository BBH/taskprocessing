## Exemplary app for asynchronous tasks processing
* http://localhost:8080/tasks/ is used to create a task; you get the task id in return
* http://localhost:8080/tasks/taskId is used to track the processing of the task with id taskId; 
it takes 25 seconds to process the task from the moment it's created, you can see the progress (in %)
and when it's finished you can also see the result 

### Requirements:
* java17
* docker
* docker-compose

### Create and run:
* ./gradlew bootBuildImage
* docker-compose up

### Run tests:
./gradlew test

### How to use API:
* create a task:

curl --location --request POST 'localhost:8080/tasks/'
  --header 'Content-Type: application/json' 
  --data-raw '{ "base": "2", "exponent": "10" }'
* get the task with given id: 

curl --location --request GET 'localhost:8080/tasks/taskId'

(replace taskId with the id returned by POST)

### You can also test API via Swagger:
http://localhost:8080/swagger-ui.html