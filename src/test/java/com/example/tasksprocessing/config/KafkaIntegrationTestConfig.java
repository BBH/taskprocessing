package com.example.tasksprocessing.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.config.TopicBuilder;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;

import static com.example.tasksprocessing.infrastructure.Profiles.INTEGRATION_TEST;
import static com.example.tasksprocessing.infrastructure.kafka.KafkaConfig.TOPIC;


@Configuration
@Profile(INTEGRATION_TEST)
public class KafkaIntegrationTestConfig {

    public static final String TESTCONTAINERS_DOCKER_IMAGE = "confluentinc/cp-kafka:6.2.1";
    public static final int NUMBER_OF_PARTITIONS = 10;

    @Bean
    String kafkaBootstrapServer() {
        KafkaContainer kafkaContainer = new KafkaContainer(DockerImageName.parse(TESTCONTAINERS_DOCKER_IMAGE));
        kafkaContainer.start();
        createExampleTopic();
        return kafkaContainer.getBootstrapServers();
    }

    private void createExampleTopic() {
        TopicBuilder.name(TOPIC)
                .partitions(NUMBER_OF_PARTITIONS)
                .build();
    }


}
