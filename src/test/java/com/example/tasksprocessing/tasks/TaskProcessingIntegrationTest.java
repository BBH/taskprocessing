package com.example.tasksprocessing.tasks;

import com.example.tasksprocessing.config.IntegrationTest;
import com.example.tasksprocessing.tasks.infrastructure.dtos.Status;
import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskIncoming;
import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskOutgoing;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static com.example.tasksprocessing.tasks.infrastructure.dtos.Status.FINISHED;
import static com.example.tasksprocessing.tasks.infrastructure.dtos.Status.RUNNING;
import static org.assertj.core.api.Assertions.assertThat;
import static org.testcontainers.shaded.org.awaitility.Awaitility.await;

public class TaskProcessingIntegrationTest extends IntegrationTest {

    @Autowired
    TasksFacade facade;

    @Test
    void shouldProcessTaskCorrectly() {
        //given
        TaskIncoming taskIncoming = new TaskIncoming(2, 10);
        //when
        String id = facade.addTask(taskIncoming);
        //then
        TaskOutgoing processingResult = waitForStatus(id, RUNNING);
        assertThat(processingResult.getProgress()).isBetween(1, 99);
        //and then
        processingResult = waitForStatus(id, FINISHED);
        assertThat(processingResult.getProgress()).isEqualTo(100);
        assertThat(processingResult.getResult()).isEqualTo(1024);
    }

    private TaskOutgoing waitForStatus(String id, Status status) {
        AtomicReference<Optional<TaskOutgoing>> result = new AtomicReference<>(null);
        await().untilAsserted(() -> {
                    result.set(facade.findTaskById(id));
                    assertThat(result.get().isPresent()).isTrue();
                    assertThat(result.get().get().getStatus()).isEqualTo(status);

                }
        );

        return result.get().get();
    }
}
