package com.example.tasksprocessing.tasks;

import com.example.tasksprocessing.config.IntegrationTest;
import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskIncoming;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.concurrent.atomic.AtomicReference;

import static com.example.tasksprocessing.tasks.infrastructure.dtos.Status.FINISHED;
import static com.example.tasksprocessing.tasks.infrastructure.dtos.Status.RUNNING;
import static net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.testcontainers.shaded.org.awaitility.Awaitility.await;


public class TaskRestControllerIntegrationTest extends IntegrationTest {

    @Autowired
    TasksFacade facade;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    MockMvc mockMvc;

    @Test
    void shouldPostNewValidTask() throws Exception {
        //given
        TaskIncoming taskIncoming = new TaskIncoming(2, 10);
        //when
        MvcResult mvcResult = mockMvc
                .perform(post(
                        "/tasks")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(taskIncoming)))
                .andExpect(status().isCreated())
                .andReturn();
        //then
        String taskId = JsonPath.read(mvcResult.getResponse().getContentAsString(), "$.id");
        assertThat(facade.findTaskById(taskId)).isNotEmpty();
    }

    @Test
    void shouldReturnBadRequestAndInfoIfFieldsTooHigh() throws Exception {
        //given
        TaskIncoming taskIncoming = new TaskIncoming(10, 25);
        //expect
        mockMvc
                .perform(post("/tasks")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(taskIncoming)))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.base", Is.is("must be less than or equal to 8")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.exponent", Is.is("must be less than or equal to 20")));
    }

    @Test
    void shouldReturnBadRequestAndInfoIfFieldsTooLow() throws Exception {
        //given
        TaskIncoming taskIncoming = new TaskIncoming(1, -1);
        //expect
        mockMvc
                .perform(post("/tasks")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(taskIncoming)))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.base", Is.is("must be greater than or equal to 2")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.exponent", Is.is("must be greater than or equal to 2")));
    }

    @Test
    void shouldReturn404IfTaskNotFound() throws Exception {
        //expect
        mockMvc
                .perform(get("/tasks/invalidTaskId"))
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldReturnValidTaskInfo() {
        //when
        TaskIncoming taskIncoming = new TaskIncoming(2, 10);
        String taskId = facade.addTask(taskIncoming);
        //then
        AtomicReference<String> result = new AtomicReference<>("");
        await().untilAsserted(() -> {
            result.set(getResponseFor(taskId));
            assertThatJson(result.get())
                    .and(
                            content -> content.node("status").isEqualTo(RUNNING),
                            content -> content.node("result").isAbsent()
                    );
        });
        //and then
        await().untilAsserted(() -> {
            result.set(getResponseFor(taskId));
            assertThatJson(result.get())
                    .and(
                            content -> content.node("status").isEqualTo(FINISHED),
                            content -> content.node("result").isEqualTo(1024),
                            content -> content.node("progress").isEqualTo(100)
                    );
        });


    }

    private String getResponseFor(String taskId) throws Exception {
        return mockMvc
                .perform(get("/tasks/" + taskId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString();
    }
}
