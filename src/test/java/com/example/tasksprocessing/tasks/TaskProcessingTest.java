package com.example.tasksprocessing.tasks;

import com.example.tasksprocessing.tasks.infrastructure.dtos.Status;
import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskIncoming;
import com.example.tasksprocessing.infrastructure.kafka.EventSender;
import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskOutgoing;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static com.example.tasksprocessing.tasks.TasksFacadeConfiguration.createFacade;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

public class TaskProcessingTest {

    EventSender taskEventSender = mock(EventSender.class);
    TasksFacade facade = createFacade(taskEventSender, 1);

    @Test
    void shouldReturnResultWhenTaskIsFinished() {
        //given
        TaskIncoming taskIncoming = new TaskIncoming(2, 10);
        String taskId = facade.addTask(taskIncoming);
        //when
        facade.processTask(taskId, taskIncoming);
        //then
        Optional<TaskOutgoing> taskById = facade.findTaskById(taskId);
        assertThat(taskById.isPresent()).isTrue();
        //and then
        TaskOutgoing processingResult = taskById.get();
        assertThat(processingResult.getStatus()).isEqualTo(Status.FINISHED);
        assertThat(processingResult.getProgress()).isEqualTo(100);
        assertThat(processingResult.getResult()).isEqualTo(1024);
    }

}
