package com.example.tasksprocessing.tasks;

import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskIncoming;
import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskOutgoing;
import com.example.tasksprocessing.tasks.infrastructure.kafka.sender.TaskEventSender;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static com.example.tasksprocessing.tasks.infrastructure.dtos.Status.WAITING;
import static com.example.tasksprocessing.tasks.TasksFacadeConfiguration.createFacade;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TaskBasicTest {

    TaskEventSender taskEventSender = mock(TaskEventSender.class);
    TasksFacade facade = createFacade(taskEventSender, 1);

    @Test
    void shouldAddReceivedTaskForProcessing() {
        //given
        TaskIncoming taskIncoming = new TaskIncoming(2, 10);
        //when
        String taskId = facade.addTask(taskIncoming);
        //then
        assertThat(facade.findTaskById(taskId)).isNotEmpty();
        verify(taskEventSender).send(taskId, taskIncoming);
    }

    @Test
    void shouldReturnTaskWithGivenId() {
        //given
        TaskIncoming taskIncoming = new TaskIncoming(2, 10);
        String taskId = facade.addTask(taskIncoming);
        //when
        Optional<TaskOutgoing> task = facade.findTaskById(taskId);
        //then
        assertThat(task.isPresent()).isTrue();
        assertThat(task.get().getStatus()).isEqualTo(WAITING);
        assertThat(task.get().getProgress()).isEqualTo(0);
        assertThat(task.get().getResult()).isEqualTo(null);
    }

    @Test
    void shouldReturnAnEmptyOptionalWhenTaskWithIdNotFound() {
        //given
        //when
        Optional<TaskOutgoing> task = facade.findTaskById("invalidTaskId");
        //then
        assertThat(task.isEmpty()).isTrue();
    }


}
