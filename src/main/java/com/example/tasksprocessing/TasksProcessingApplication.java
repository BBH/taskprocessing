package com.example.tasksprocessing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TasksProcessingApplication {

    public static void main(String[] args) {
        SpringApplication.run(TasksProcessingApplication.class, args);
    }

}
