package com.example.tasksprocessing.tasks;

import com.example.tasksprocessing.tasks.infrastructure.dtos.Status;
import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskIncoming;
import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskOutgoing;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static com.example.tasksprocessing.tasks.infrastructure.dtos.Status.*;
import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static java.lang.Thread.sleep;

@Getter
@AllArgsConstructor
class ExponentiationTask {

    String id;
    Status status = WAITING;
    int progress = 0;
    @JsonInclude(NON_NULL)
    Integer result = null;

    public ExponentiationTask(String id) {
        this.id = id;
    }

    TaskOutgoing dto() {
        return new TaskOutgoing(id, status, progress, result);
    }

    void processTask(TaskIncoming taskIncoming, int delay) {
        status = RUNNING;
        processTaskWith(delay);
        status = FINISHED;
        progress = 100;
        result = (int) Math.pow(taskIncoming.getBase(), taskIncoming.getExponent());
    }

    private void processTaskWith(int delay) {
        for (int i = 1; i <= 100; i++) {
            progress=i;
            try {
                sleep(delay / 100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
