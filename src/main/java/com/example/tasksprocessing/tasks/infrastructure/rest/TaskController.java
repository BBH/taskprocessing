package com.example.tasksprocessing.tasks.infrastructure.rest;

import com.example.tasksprocessing.tasks.TasksFacade;
import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskIncoming;
import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskOutgoing;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class TaskController {

    private final TasksFacade facade;

    @PostMapping("/tasks")
    ResponseEntity<String> createTask(@Valid @RequestBody TaskIncoming task) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(String.format("{\"id\": \"%s\"}", facade.addTask(task)));
    }

    @GetMapping("/tasks/{taskId}")
    ResponseEntity<TaskOutgoing> getTaskWithId(@PathVariable String taskId) {
        Optional<TaskOutgoing> task = facade.findTaskById(taskId);
        return task
                .map(taskOutgoing -> ResponseEntity
                        .status(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(taskOutgoing))
                .orElseGet(() -> ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .body(null));
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}

