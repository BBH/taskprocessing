package com.example.tasksprocessing.tasks.infrastructure.kafka.sender;

import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskIncoming;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.KafkaTemplate;

import static com.example.tasksprocessing.infrastructure.kafka.KafkaFactories.kafkaTemplate;

@Configuration
@EnableKafka
public class TaskEventSenderConfig {

    @Bean
    KafkaTemplate<String, TaskIncoming> taskEventKafkaTemplate(String kafkaBootstrapServer, ObjectMapper objectMapper) {
        return kafkaTemplate(kafkaBootstrapServer, objectMapper);
    }

    @Bean
    TaskEventSender taskEventSender(KafkaTemplate<String, TaskIncoming> kafkaTemplate) {
        return new TaskEventSender(kafkaTemplate);
    }

}
