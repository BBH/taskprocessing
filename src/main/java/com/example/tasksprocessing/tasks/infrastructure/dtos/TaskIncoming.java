package com.example.tasksprocessing.tasks.infrastructure.dtos;

import lombok.Value;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Value
public class TaskIncoming {
    @NotNull
    @Min(2)
    @Max(8)
    int base;

    @NotNull
    @Min(2)
    @Max(20)
    int exponent;
}
