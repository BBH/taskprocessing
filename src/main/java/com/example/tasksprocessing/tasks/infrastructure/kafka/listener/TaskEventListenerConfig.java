package com.example.tasksprocessing.tasks.infrastructure.kafka.listener;

import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskIncoming;
import com.example.tasksprocessing.tasks.TasksFacade;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.listener.DefaultErrorHandler;

import static com.example.tasksprocessing.infrastructure.kafka.KafkaFactories.kafkaListenerContainerFactory;

@Configuration
@EnableKafka
public class TaskEventListenerConfig {

    public static final String TASKS_LISTENER_CONTAINER_FACTORY = "TasksListenerContainerFactory";
    public static final String GROUP_ID = "kafkaListener";
    public static final int CONCURRENCY_LEVEL = 10;

    @Bean
    TaskEventListener taskEventListener(TasksFacade facade) {
        return new TaskEventListener(facade);
    }

    @Bean(TASKS_LISTENER_CONTAINER_FACTORY)
    ConcurrentKafkaListenerContainerFactory<String, TaskIncoming> taskEventListenerContainerFactory(String kafkaBootstrapServer, ObjectMapper objectMapper) {
        return kafkaListenerContainerFactory(kafkaBootstrapServer, TaskIncoming.class, objectMapper, CONCURRENCY_LEVEL, new DefaultErrorHandler());
    }
}


