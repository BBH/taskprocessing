package com.example.tasksprocessing.tasks.infrastructure.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Value;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Value
public class TaskOutgoing {
    String id;
    Status status;
    int progress;
    @JsonInclude(NON_NULL)
    Integer result;
}
