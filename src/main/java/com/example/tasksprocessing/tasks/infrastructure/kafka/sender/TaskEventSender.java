package com.example.tasksprocessing.tasks.infrastructure.kafka.sender;

import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskIncoming;
import com.example.tasksprocessing.infrastructure.kafka.EventSender;
import lombok.AllArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;

import static com.example.tasksprocessing.infrastructure.kafka.KafkaConfig.TOPIC;

@AllArgsConstructor
public class TaskEventSender implements EventSender {

    KafkaTemplate<String, TaskIncoming> kafkaTemplate;

    @Override
    public void send(String key, TaskIncoming taskIncoming) {
        kafkaTemplate.send(TOPIC, key, taskIncoming);
    }
}
