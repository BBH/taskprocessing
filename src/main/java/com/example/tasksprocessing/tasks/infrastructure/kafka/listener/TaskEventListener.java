package com.example.tasksprocessing.tasks.infrastructure.kafka.listener;

import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskIncoming;
import com.example.tasksprocessing.tasks.TasksFacade;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import static com.example.tasksprocessing.infrastructure.kafka.KafkaConfig.TOPIC;
import static com.example.tasksprocessing.tasks.infrastructure.kafka.listener.TaskEventListenerConfig.GROUP_ID;
import static com.example.tasksprocessing.tasks.infrastructure.kafka.listener.TaskEventListenerConfig.TASKS_LISTENER_CONTAINER_FACTORY;


@Slf4j
@RequiredArgsConstructor
@KafkaListener(topics = {TOPIC}, groupId = GROUP_ID, containerFactory = TASKS_LISTENER_CONTAINER_FACTORY)
public class TaskEventListener {

    private final TasksFacade facade;

    @KafkaHandler
    public void receive(@Payload TaskIncoming task, @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String key) {
        log.info("Event received: {}", task);
        facade.processTask(key, task);
    }
}
