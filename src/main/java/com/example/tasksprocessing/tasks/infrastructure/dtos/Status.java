package com.example.tasksprocessing.tasks.infrastructure.dtos;

public enum Status {
    WAITING, RUNNING, FINISHED
}
