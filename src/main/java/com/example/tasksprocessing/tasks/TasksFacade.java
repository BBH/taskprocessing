package com.example.tasksprocessing.tasks;

import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskIncoming;
import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskOutgoing;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class TasksFacade {

    private final TasksService tasksService;

    public String addTask(TaskIncoming taskIncoming) {
        return tasksService.addTask(taskIncoming);
    }

    public Optional<TaskOutgoing> findTaskById(String taskId) {
        return tasksService.findTaskById(taskId);
    }

    public void processTask(String taskId, TaskIncoming taskIncoming) {
        tasksService.processTask(taskId, taskIncoming);
    }
}
