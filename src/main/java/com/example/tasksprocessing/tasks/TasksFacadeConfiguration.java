package com.example.tasksprocessing.tasks;

import com.example.tasksprocessing.infrastructure.kafka.EventSender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class TasksFacadeConfiguration {

    static TasksFacade createFacade(EventSender eventSender, int delay) {
        TaskRepository repository = new TaskInMemoryRepository();
        TasksService tasksService = new TasksService(eventSender, repository, delay);
        return new TasksFacade(tasksService);
    }

    @Bean
    TasksFacade createTasksFacade(EventSender eventSender, @Value("${delay}") int delay) {
        return createFacade(eventSender, delay);
    }
}
