package com.example.tasksprocessing.tasks;

import java.util.Optional;

interface TaskRepository {

    ExponentiationTask save(ExponentiationTask task);

    Optional<ExponentiationTask> findById(String taskId);

}
