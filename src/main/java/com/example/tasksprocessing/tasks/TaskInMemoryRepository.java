package com.example.tasksprocessing.tasks;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

class TaskInMemoryRepository implements TaskRepository {
    final ConcurrentHashMap<String, ExponentiationTask> tasks = new ConcurrentHashMap<>();

    @Override
    public ExponentiationTask save(ExponentiationTask task) {
        tasks.put(task.getId(), task);
        return task;
    }

    @Override
    public Optional<ExponentiationTask> findById(String taskId) {
        return Optional.ofNullable(tasks.get(taskId));
    }
}
