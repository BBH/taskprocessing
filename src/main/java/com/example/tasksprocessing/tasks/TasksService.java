package com.example.tasksprocessing.tasks;

import com.example.tasksprocessing.infrastructure.kafka.EventSender;
import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskIncoming;
import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskOutgoing;
import lombok.RequiredArgsConstructor;

import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
class TasksService {

    private final EventSender taskEventSender;
    private final TaskRepository repository;
    private final int delay;

    public String addTask(TaskIncoming taskIncoming) {
        String key = UUID.randomUUID().toString();
        ExponentiationTask task = new ExponentiationTask(key);
        repository.save(task);
        taskEventSender.send(key, taskIncoming);
        return key;
    }

    public Optional<TaskOutgoing> findTaskById(String taskId) {
        return repository.findById(taskId).map(ExponentiationTask::dto);

    }

    public void processTask(String taskId, TaskIncoming taskIncoming) {
        repository.findById(taskId).ifPresent(task -> task.processTask(taskIncoming, delay));
    }
}



