package com.example.tasksprocessing.infrastructure.kafka;

import com.example.tasksprocessing.tasks.infrastructure.dtos.TaskIncoming;

public interface EventSender {

    void send(String key, TaskIncoming taskIncoming);
}
