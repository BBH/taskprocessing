package com.example.tasksprocessing.infrastructure.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.kafka.annotation.EnableKafka;

import static com.example.tasksprocessing.infrastructure.Profiles.NOT_INTEGRATION_TEST;
import static com.fasterxml.jackson.annotation.JsonCreator.Mode.PROPERTIES;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;

@Configuration
@EnableKafka
public class KafkaConfig {
    public static final String TOPIC = "tasks";

    @Bean
    @Primary
    public ObjectMapper objectMapper() {
        return new Jackson2ObjectMapperBuilder()
                .modules(new ParameterNamesModule(PROPERTIES), new Jdk8Module(), new JavaTimeModule())
                .featuresToDisable(WRITE_DATES_AS_TIMESTAMPS)
                .failOnUnknownProperties(false)
                .build();
    }

    @Bean
    @Profile(NOT_INTEGRATION_TEST)
    String kafkaBoostrapServer(@Value("${kafka.boostrap-server}") String boostrapServer) {
        return boostrapServer;
    }
}